/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 11:15:01 by mmeisson          #+#    #+#             */
/*   Updated: 2019/03/08 11:44:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft.h"

static int		check_newline(char *remain, char **line)
{
	char	*newline;

	newline = NULL;
	if (remain != NULL)
		newline = ft_strchr(remain, '\n');
	if (newline != NULL)
	{
		*line = ft_strsub(remain, 0, newline - remain);
		if (*line == NULL)
			return (-1);
		ft_memmove(remain, newline + 1, ft_strlen(newline));
		return (1);
	}
	return (0);
}

static t_file	*get_file(t_list **files, int fd)
{
	t_file		*tmp;
	t_file		new;
	t_list		*iter;

	iter = *files;
	while (iter != NULL)
	{
		tmp = (t_file *)iter->content;
		if (fd == tmp->fd)
		{
			return (tmp);
		}
		iter = iter->next;
	}
	new.remain = NULL;
	new.fd = fd;
	iter = ft_lstnew(&new, sizeof(new));
	if (iter != NULL)
	{
		ft_lstadd(files, iter);
		return ((t_file *)iter->content);
	}
	return (NULL);
}

static void		remove_file(t_list **files, int fd)
{
	t_list	*prev;
	t_list	*iter;
	t_file	*tmp;

	prev = NULL;
	iter = *files;
	while (iter != NULL)
	{
		tmp = iter->content;
		if (tmp->fd == fd)
		{
			if (prev == NULL)
				*files = iter->next;
			else
				prev->next = iter->next;
			ft_strdel(&tmp->remain);
			free(tmp);
			free(iter);
			break ;
		}
		prev = iter;
		iter = iter->next;
	}
}

static int		post_return(
	t_list **files,
	t_file *current,
	char **line,
	int fd
)
{
	int		ret;

	if (current->remain == NULL || *current->remain == '\0')
	{
		remove_file(files, fd);
		ret = 0;
	}
	else if ((ret = check_newline(current->remain, line) == 0))
	{
		*line = current->remain;
		current->remain = NULL;
		ret = 1;
	}
	return (ret);
}

int				get_next_line(int fd, char **line)
{
	char			buffer[BUFF_SIZE + 1];
	static t_list	*files = NULL;
	t_file			*current;
	ssize_t			ret;

	current = get_file(&files, fd);
	if ((ret = check_newline(current->remain, line) != 0))
		return (ret);
	while ((ret = read(fd, buffer, BUFF_SIZE)) > 0)
	{
		buffer[ret] = 0;
		current->remain = ft_strover(current->remain, buffer);
		if ((ret = check_newline(current->remain, line) != 0))
			return (ret);
	}
	if (ret == -1)
	{
		remove_file(&files, fd);
		return (-1);
	}
	return (post_return(&files, current, line, fd));
}
