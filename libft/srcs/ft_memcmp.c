/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:54 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 14:05:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdint.h>

static int	ft_smallpart(const uint8_t *s1, const uint8_t *s2, size_t n)
{
	size_t				i;

	i = 0;
	while (i < n)
	{
		if (s1[i] != s2[i])
			return (s1[i] - s2[i]);
		i++;
	}
	return (0);
}

static int	ft_bigpart(const uint64_t *s1, const uint64_t *s2, size_t big_index)
{
	size_t		i;

	i = 0;
	while (i < big_index)
	{
		if (s1[i] != s2[i])
		{
			return (ft_smallpart(
				(void *)((uint64_t *)s1 + i),
				(void *)((uint64_t *)s2 + i),
				sizeof(uint64_t)));
		}
		i++;
	}
	return (0);
}

int			ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t		big;
	size_t		small;
	int			cmp;

	if (n == 0)
	{
		return (0);
	}
	big = n / sizeof(uint64_t);
	small = n % sizeof(uint64_t);
	cmp = 0;
	if (big > 0)
	{
		cmp = ft_bigpart(s1, s2, big);
	}
	if (cmp == 0)
	{
		cmp = ft_smallpart(
			s1 + (big * sizeof(uint64_t)),
			s2 + (big * sizeof(uint64_t)),
			small);
	}
	return (cmp);
}
