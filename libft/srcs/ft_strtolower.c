/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtolower.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 12:26:49 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/22 12:26:51 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		lower(char *s)
{
	*s = ft_tolower(*s);
}

char			*ft_strtolower(char *str)
{
	ft_striter(str, lower);
	return (str);
}
