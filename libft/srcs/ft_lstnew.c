/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:12 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/13 19:45:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(const void *content, size_t content_size)
{
	t_list	*node;

	if (!(node = malloc(sizeof(*node))))
		return (NULL);
	if (content != NULL && content_size != 0)
	{
		if (!(node->content = ft_memdup(content, content_size)))
		{
			free(node);
			return (NULL);
		}
		node->content_size = content_size;
	}
	node->next = NULL;
	return (node);
}
